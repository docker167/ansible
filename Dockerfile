FROM python:3.10.0rc1-buster

LABEL maintainer="bydanav@gmail.com"

ARG ARCH=amd64

ENV ANSIBLE_FOLDER="/etc/ansible"
ENV DEBIAN_FRONTEND="noninteractive"
ENV ANSIBLE_VERSION="10.1.0"
ENV ANSIBLE_LINT_VERSION="24.6.0"

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      gnupg2 \
      sshpass \
      git \
      openssh-client \
    && \
    rm -rf /var/lib/apt/lists/* && \
    apt-get clean

COPY  requirements.txt /tmp

RUN sed -i "s/{ANSIBLE_VERSION}/$ANSIBLE_VERSION/; s/{ANSIBLE_LINT_VERSION}/$ANSIBLE_LINT_VERSION/" /tmp/requirements.txt && \
    python3 -m pip install --upgrade pip && \
    python3 -m pip install --no-cache-dir -r /tmp/requirements.txt && \
    mkdir -p ${ANSIBLE_FOLDER} && \
    mkdir -p /root/.ssh && \
    chown 600 /root/.ssh && \
    rm -rf /tmp/*

COPY ./files/ansible_files/ ${ANSIBLE_FOLDER}/

WORKDIR ${ANSIBLE_FOLDER}

CMD [ "ansible-playbook", "--version" ]


